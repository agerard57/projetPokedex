# projetPokedex

Ce projet est le projet tutoré du 3ème semestre.




## Sujet Pokédex 
Le but de ce projet est de créer une encyclopédie regroupant toutes les informations relatives aux créatures dénommées Pokémon.
Un certain nombre d'interactions sont prévues comme par exemple, la gestion de favoris, la recherche avancée ou encore l’export PDF. 
Le cahier des charges complet par rapport aux besoins applicatifs sera présenté après la sélection de ce sujet et pourra être adapté en fonction de l’avancée générale du projet. Le principal objectif de ce sujet est de vous faire manipuler des technologies et des concepts que vous n’avez pas forcément approfondi en DUT jusqu’à présent, tout en utilisant des méthodologies et outils fortement utilisés dans le monde informatique moderne. 
## Précisions techniques 
Dans ce projet, vous serez amenés à faire un sujet qui paraît relativement simple, mais vous serez amenés à manipuler une panoplie de technologies différentes pour le réaliser, ainsi que d’utiliser des outils avancés pour vous aider à gérer votre projet. 

N’hésitez pas à vous renseigner sur tous les points que vous ne comprenez pas ci-dessous. 
### Technologies 
● Toutes vos données seront stockées dans une base de données ElasticSearch 
● Vous aurez comme source de données toute une panoplie de fichiers en csv qui regroupent virtuellement toutes les informations sur les Pokémons et leur univers. Vous devrez concevoir un script dans le langage de votre choix (Python, Node…) pour les parser et les adapter pour fonctionner avec votre base de données ElasticSearch 
● Vous devrez créer une application back dans la langue de votre choix (PHP, Node…) qui fera office d’API pour exploiter les données de votre base de données ElasticSearch 
● Votre application front doit tourner sur browser moderne (Chrome/FF). 
● L’application doit être conçue en respectant les principes d’une Progressive Web App (PWA) pour permettre “l'installation” de l’application sur Windows 10 et Android. 
● Votre application front doit tourner sur base de ReactJS version 16+. Vous devrez faire usage de composants fonctionnels et de hooks. 
● Pour votre Front-End, nous vous recommandons fortement d’utiliser une librairie graphique (par exemple Material-UI ou Ant Design) 
### Gestion de projet
● Votre projet devra être réalisé impérativement avec l’aide de l’outil de versionnage GIT. Vous serez amenés à créer plusieurs répos pour vos divers projets (back, front et script de données). 
● Vous utiliserez GitLab pour gérer vos répos, et utiliserez le pattern des “Merge Requests” (aussi connu sous le nom de Pull Requests). 
● Nous vous demanderons d’utiliser l’outil “Trello” pour effectuer un suivi visuel du travail accompli et à faire. 
### Autres points… 
● Nous allons vous présenter des maquettes de l’application pour vous servir de modèle à respecter lors de l’implémentation de votre front-end 
● Vu que votre application est une PWA, vous devrez faire attention à la compatibilité sur téléphone mobile également.
```
